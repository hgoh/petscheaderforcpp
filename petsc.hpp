#ifndef PETSC_HPP
#define PETSC_HPP

#include <petsc.h>

using boolean = PetscBool;
using integer = PetscInt;
using real    = PetscReal;
using scalar  = PetscScalar;			
using ecode   = PetscErrorCode;

const auto TRUE  = PETSC_TRUE;
const auto FALSE = PETSC_FALSE;
const auto PI    = PETSC_PI;
const auto iI    = PETSC_i;

template <typename T1, typename... T2>
inline auto wprintf(T1&& a, T2&&... b){
  return PetscPrintf(PETSC_COMM_WORLD,a,b...);
}

template <typename T1, typename... T2>
inline auto syprintf(T1&& a, T2&&... b){
  return PetscSynchronizedPrintf(PETSC_COMM_WORLD,a,b...);
}

inline auto sypflush(){
  return PetscSynchronizedFlush(PETSC_COMM_WORLD,PETSC_STDOUT);
}

template <typename T>
inline auto UNUSED(T&& a){
  return (void)(a);
}
  

#endif
